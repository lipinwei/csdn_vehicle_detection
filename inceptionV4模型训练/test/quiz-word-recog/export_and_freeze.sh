�   p         Q     TEMP_GRAPH_FILE=/tmp/inception_v4_inf_graph.pb
DATASET_DIR='/home/zjy/test/车辆检测/train'
DATASET_NAME='cars'
OUTPUT_GRAPH='./freezed.pb'
python3 export_inference_graph.py \
  --model_name=inception_v4 \
  --output_file=$TEMP_GRAPH_FILE \
  --dataset_dir=$DATASET_DIR \
  --dataset_name=$DATASET_NAME

python3 freeze_graph.py \
  --input_graph=$TEMP_GRAPH_FILE \
  --input_checkpoint=/home/zjy/test/train_dir/model.ckpt-85709 \
  --output_node_names=output,final_probs \
  --input_binary=True \
  --output_graph=$OUTPUT_GRAPH
  
cp ${DATASET_DIR%*/}/labels.txt ${O