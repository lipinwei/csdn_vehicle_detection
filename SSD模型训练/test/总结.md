
# read me

## 介绍
利用slim框架和object_detection框架，做一个物体检测的模型。通过这个作业，学员可以了解到物体检测模型的数据准备，训练和验证的过程。通过这个项目，可以了解到物体检测模型的数据准备，训练和验证的过程。

## 数据集
下载连接：https://gitee.com/wangteao/computer-vision
这个项目中用的到数据集是从码云上面下载的标注好了的车的数据集（数据集内部的person，car，bus等的标定框坐标都写在了annotations里面）需要注意的是，我们这个项目只是用来检测car的，所以labels_items.txt里面的类只有car。

数据集中各目录如下：

- images，图片目录，数据集中所有图片都在这个目录下面。
- annotations/xmls，针对图片中的物体，使用LabelImg工具标注产生的xml文件都在这里，每个xml文件对应一个图片文件，每个xml文件里面包含图片中多个物体的位置和种类信息。

## 代码
本次代码使用tensorflow官方代码，代码地址如下：
https://github.com/tensorflow/models/tree/r1.5

> 因为最新的代码做了一些变化，需要使用pycocotool这个库，但是这个库的安装很复杂，目前暂时无法在tinymind上进行，所以这里使用比较老版本的代码

主要使用的是research/object_detection目录下的物体检测框架的代码。这个框架同时引用slim框架的部分内容，需要对运行路径做一下设置，不然会出现找不到module的错误。
设置运行路径的方式有两种：

- 直接在代码中插入路径，使用**sys.path.insert**，具体内容留作学员作业，请自行查找相关资料

- 使用环境变量PYTHONPATH，参考https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/installation.md 

### 设置环境变量(第一个坑，这个坑每次运行.py时候都要用)
cd research文件夹下面

然后命令行输入 

export PYTHONPATH=$PYTHONPATH:`pwd`:`pwd`/slim

这里的pwd指research的绝对路径：/home/zjy/test/model/research

### 预编译
代码中需要预先编译一些proto buffer的class，不然会出现如下错误
```
Traceback (most recent call last):
  File "Data_preprocessing.py", line 16, in <module>
    from object_detection.utils import label_map_util
  File "/path/to/object_detection/utils/label_map_util.py", line 22, in <module>
    from object_detection.protos import string_int_label_map_pb2
ImportError: cannot import name string_int_label_map_pb2
```
解决方式是直接在research目录下运行如下代码
```sh
sudo apt install protobuf-compiler
protoc object_detection/protos/*.proto --python_out=.
```

### 预训练模型
object_detection框架提供了一些预训练的模型以加快模型训练的速度，不同的模型及检测框架的预训练模型不同，常用的模型有resnet，mobilenet以及最近google发布的nasnet，检测框架有faster_rcnn，ssd等，本次作业使用mobilenet模型ssd检测框架，其预训练模型请自行在model_zoo中查找:
https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/detection_model_zoo.md

>ssd论文：https://arxiv.org/abs/1512.02325

>mobilenet论文：https://arxiv.org/abs/1704.04861

# 注意（之前总是生成不了tfrecord）
## 数据集转成tfrecord
因为我们这里只需要car类，也就是annotations里面的xml中name==‘car’的信息，所以需要求该create_data.py

需要加入两行

>ob_name = obj['name']

>if ob_name == 'car':

然后：

   1. 设置环境变量
   2. 预编译
   3. 执行create_data.py
   >python3 /home/zjy/.../create_data.py --data_dir=/home/.../quiz-w8-data  --label_map_path=/home/.../labels_items.txt --output_dir=/home/.../quiz-w8-data
   >其中labels_items.txt里面只有car，output_dir是输出tfrecord文件的目录
   
## 修改ssd_mobilenet_v1_pets.config文件参数
   1. 为什么使用config文件？
   >因为目标检测需要在命令行输入很多的参数，所以都写入到config里面，该项目提供的config文件是修改自detection里面sample下的config目录下的文件。
   2. num_classes:修改为labels_items里面的类数量
   3. image_resizer{} 修改输入图片的大小
   4. train_config 中用的优化器为optimizer
   5. fine_tune_checkpoint: '预训练模型+名'
   6. batch_size:
   7. num_steps: 训练步数
   8. train_input_reader: 
   >tfrecord的全路径
   >labels_items.txt的全路径
   9. eval_config: num_examples:47(计算出来的，总数减去训练个数) max_evals:(验证次数)
   10. eval_input_reader
## 下载预训练模型
在objec_detection/g3doc/detection_model_zoo.md中挑选在coo上预训练的模型，本项目使用的是轻量的ssd_mobilenet_v1_coo，使用下来，检测效果不是很好，后续可以尝试改用其他的预训练模型，如果要改的话，估计要修改config文件等。

## 训练，验证，保存模型，使用模型
### 数据准备完成
学员的模型引用的数据集中，应包含以下文件，文件名需要跟此处文件名一致。
- model.ckpt.data-00000-of-00001  预训练模型相关文件
- model.ckpt.index  预训练模型相关文件
- model.ckpt.meta  预训练模型相关文件
- labels_items.txt  数据集中的label_map文件
- pet_train.record  数据准备过程中，从原始数据生成的tfrecord格式的数据
- pet_val.record  数据准备过程中，从原始数据生成的tfrecord格式的数据
- test.jpg  验证图片，取任意一张训练集图片即可
## 训练步骤
   1. 设置环境变量
   2. 启动训练
   >python3 /home/.../object_detection/train.py --train_dir=(存放ckpt和summary文件的地址)./train_dir --pipline_config_path=/home/.../ssd_..config
   3. 执行验证
   >设置环境变量
   >python3 /home/.../object_detection/eval.py --checkpoint_dir=./train_dir --eval_dir=(验证生成的summary)./eval_dir --pipline_config_path=/home/.../ssd_..config
   >需要注意的是，在命令行并不会有什么输出信息，需要去tensorboard查看
   4. 模型导出
   >设置环境变量
   >python3 /home/.../export_inference_graph.py --input_type=image_tensor(这个是固定写法) --pipline_config_path=/home/.../ssd_..config --trained_checkpoint_prefix=./train_dir/model.ckpt-500 --output_directory=./exported(输出pb文件的目录)
   5. 使用导出的模型
   >需要将inference.py放入object_detection目录下
   >python3 /home/inference.py --output_dir=./exported(用于导出测试的结果图output.png) --dataset_dir=./data(需要在dataset_dir中放入一张名为test.jpg的图片)
   



## 需要改进的地方
### 更换使用的检测模型
### 用GUI或者WEB测试图片


```python

```
