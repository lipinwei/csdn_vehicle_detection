# CSDN车辆检测

#### 介绍
CSDN车辆检测项目汇总

#### 软件架构
pycharm ubuntu16.04 python3.5

#### 使用说明

1.  首先安装SSD模型训练里的总结.md里面的介绍运行模型，并生成.pb模型文件
2.  再安装inceptionV4模型训练里面的总结.md里面的介绍运行模型，并生成.pb文件
3.  将前面两个生成的模型文件，分别按照模型融合里freezed文件夹内的readme.txt文件指示，放入相应的文件夹，再按照模型融合文件下的使用说明.txt指示运行，即可完成功能使用。

#### 参与贡献
李品伟：SSD模型训练，inceptionV4模型训练，整理文件并上传资料到码云
刘瑾泉：模型融合
夏远满：项目报告书
1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
